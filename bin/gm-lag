#!/usr/bin/env ruby
# Displays the number of seconds a mirror is lagging behind

require 'optparse'
require 'time'
require 'uri'
require 'net/http'
require_relative '../lib/mirror_toolkit'

options = {}
OptionParser.new do |opts|
  opts.on('-h', '--human', 'Display human times') { |v| options[:human] = v }
  opts.on('-d', '--distfiles', 'Treat as distfiles mirror') { |v| options[:distfiles] = v }
  opts.on('-r', '--rsync', 'Treat as rsync mirror') { |v| options[:rsync] = v }
end.parse!

abort '-d and -r are exclusive.' if options[:distfiles] && options[:rsync]

lag = MirrorToolkit.get_lag(ARGV[0], options[:rsync] ? MirrorToolkit::TYPE_RSYNC : MirrorToolkit::TYPE_DISTFILES)

if lag.nil?
  puts 'unknown'
elsif options[:human]
  puts MirrorToolkit.humanize_seconds(lag)
else
  puts lag.to_i
end
